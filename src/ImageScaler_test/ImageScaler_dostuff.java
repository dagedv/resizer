package ImageScaler_test; //Info: http://www.thebuzzmedia.com/software/imgscalr-java-image-scaling-library/#download

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;

public class ImageScaler_dostuff {
	
	
	private static boolean ispositiveInteger(String str)
	{
	  return str.matches("\\d+");  
	}
	
	public static void main(String[] args) throws IOException {
		String inFilename = null;
		if(args.length<1){
			System.out.println("Usage: provide filename (jpeg) as a parameter (and optionally size as for instance 800x600 (the default) as a second parameter.)");			
		} else {			
			Integer width = 	800;
			Integer height = 	600;
			inFilename = args[0];
			
			//=== Parsing possible size parameter ===
			if(args.length==2){
				String whString = args[1];				
				if(whString.contains("x")){
					String[] parts = whString.split("x");
					if( (ispositiveInteger(parts[0]) && (ispositiveInteger(parts[1])) ) ) {
						width  = Integer.parseInt(parts[0]);
						height = Integer.parseInt(parts[1]);
					}
				}
			}
			
			BufferedImage src = ImageIO.read(new File(inFilename));
	
	        System.out.println("original size " + src.getWidth() + "x" + src.getHeight());
	        
	        //BufferedImage rotated = Scalr.rotate(src, Scalr.Rotation.CW_90, Scalr.OP_ANTIALIAS);
	        
	        BufferedImage scaled = Scalr.resize(src, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_TO_HEIGHT, 
	        		width, height, 
	        		Scalr.OP_ANTIALIAS);
	        System.out.println("scaled size " + scaled.getWidth() + "x" + scaled.getHeight());
	        
	        BufferedImage padded = new  BufferedImage(width, height, src.getType());
	        padded.createGraphics().drawImage(scaled, (BufferedImageOp) null, 
	        		(width - scaled.getWidth()) / 2 , 0);
	        
	        String outFilename =  inFilename + width + "x" + height + ".jpg";
	        ImageIO.write(padded, "jpg", new File(outFilename));
	        System.out.println("padded size " + padded.getWidth() + "x" + padded.getHeight());
		}
	}
}
