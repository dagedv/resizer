# Resizer #

![MonaLisa.jpg400x300.jpg](https://bitbucket.org/repo/dpybpj/images/4136478817-MonaLisa.jpg400x300.jpg)

This is a java based command line tool that resizes portrait format images into landscape format with padding.

The first parameter is the source image (must currently be jpeg).

The default size of the target image file is 800x600, but this can be controlled with an optional second parameter (format like 1200x900)

Versjon 0.7

It is based on the ImageScalr library.

## Usage ##

The jar file can be downloaded [from here](https://bitbucket.org/dagedv/resizer/downloads/resizer.jar)

java -jar resizer.jar MonaLisa.jpg 400x300    ... last parameter optional.

## Other ##

You are welcome to add a feature request if you have a need or preference. If it is a good idea and time + inspiration permits I will try to implement it.